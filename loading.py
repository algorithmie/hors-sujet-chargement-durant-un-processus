import itertools
import threading
import time
import sys

done = False


def animate():
    for c in itertools.cycle(['|', '/', '-', '\\']):
        if done:
            break
        sys.stdout.write('\rChargement ' + c)
        sys.stdout.flush()
        time.sleep(0.5)
    sys.stdout.write('\rTerminé !     ')


t = threading.Thread(target=animate)
t.start()

# Votre processus ici
time.sleep(10)
done = True